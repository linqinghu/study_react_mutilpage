const fs = require('fs');
const path = require('path');
const config = require('../config/index');
const entry = config.moduleName || 'pages';

const directory = path.resolve(__dirname, '../src/' + entry);
const entryList = {};

(getEntry = (dir) => {
  const entryArr = fs.readdirSync(dir);
  let pathName, filePath;
  entryArr.forEach(function (filename) {
    filePath = dir + '/' + filename
    if (fs.statSync(filePath).isDirectory()) {
      getEntry(filePath);
    } else {
      if (filename.endsWith('.js')) {
        // 不能以 / 开头，所以 + 1
        let sindex = filePath.indexOf(entry) + entry.length + 1;
        pathName = filePath.substring(sindex, filePath.lastIndexOf('.'));
        entryList[pathName] = filePath;
        //console.log(pathName + '==========' + filePath);
      }
    }
  })

})(directory);

module.exports = entryList;